//include our modules
var url = require('url');
var cors = require('cors');
var bodyParser = require('body-parser');
var express = require('express');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var serverAddress = "10.101.0.55";
var port = 1337;

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cors());

console.log('Starting server @ ' + serverAddress + ':' + port + '/');

var server = http.listen(port, serverAddress, function () {
        console.log('Server running at ' + server.address().address + ':' + server.address().port + '/');
        require('./lib/routes')(app, io);
        //app.use('/', router);

});
