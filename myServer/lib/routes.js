module.exports = function (app, io) {

    var sys = require('sys');
    var Sequelize = require('sequelize');
    var express = require('express');
    var passwordHash = require('../lib/password-hash');
    var multiparty = require('connect-multiparty');
    var multipartyMiddleware = multiparty();

    var http = require('http').Server(app);

    var router = express.Router();
    var FS = require('fs');
    var connection = new Sequelize('afekaface_db', 'root', 'tomer123');
    var separationSign = "$";
    var UsersTable;
    var PostsTable;
    var CommentsTable;
    var PostsPicturesTable;
    var clients = {};

    function getUserFromDBAsync(userEmail) {

        var promise = UsersTable.find({
            where: {
                email: userEmail
            }
        }).then(function (user) {
            
            console.log("USER FROM DB IS: " + JSON.stringify(user, null, 4));
            return user;
        });

        return promise;
    }

    function emitMessageToEveryone(messageType, message) {

        try {
            console.log("emits " + messageType + " to everyone");
            io.sockets.emit(messageType, message)
        } catch (exception) {
        }
    }

    function emitMessageToFriends(friendsArray, messageType, message) {

        console.log("FriendList of " + message.userEmail + " with size - " + friendsArray.length + " is: " + JSON.stringify(friendsArray, null, 4));
        console.log("Clients are:" + JSON.stringify(clients, null, 4));

        //try {
        for (var i = 0; i < friendsArray.length - 1; i++) {
            console.log("emits " + messageType + " to " + message.userEmail + "'s friend: " + friendsArray[i]);
            if (clients[friendsArray[i]])
                io.to(clients[friendsArray[i]].socket).emit(messageType, message)
        }
        /*        } catch (exception) {
         console.log(JSON.stringify(exception, null, 4))
         }*/
    }

    function emitMessageToSelf(selfEmail, messageType, message) {


        console.log("emits self " + messageType + " to " + selfEmail);
        io.to(clients[selfEmail].socket).emit(messageType, message);
    }


    InitilizeDB(connection);

    io.sockets.on('connection', function (socket) {

        socket.on('add-user', function (data) {
            clients[data.username] = {
                "socket": socket.id
            };

            console.log("New user and socket added: " + JSON.stringify(clients, null, 4));
        });

        //Removing the socket on disconnect
        socket.on('disconnect', function () {
            for (var name in clients) {
                if (clients[name].socket === socket.id) {
                    delete clients[name];
                    break;
                }
            }
            console.log("User and socket Removed: " + JSON.stringify(clients, null, 4));
        })
    });


    router.use(function (req, res, next) {
        console.log('%s %s', req.method, req.url);
        try {
            next();
        } catch (err) {
            //handle errors gracefully
            sys.puts(err);
            res.writeHead(500);
            res.end('Internal Server Error');
        }

    });


    router.route('/users')

        .get(function (req, res) {
            console.log("getAllUsers Function on server-post started");
            UsersTable.findAll().then(function (usersFromDB) {
                console.log("Get Users From DB: " + JSON.stringify(usersFromDB, null, 4));
                res.send(usersFromDB);
            });
        })
        .post(function (req, res) {

            req.body.password = passwordHash.generate(req.body.password);

            UsersTable.create(req.body).then(function (registeredUser) {
                //try {
                var imageBlob = FS.readFileSync("./lib/anonymous.jpg");
                //}
                //catch (exception) {
                //}
                return registeredUser.update({friendsList: ""}).then(function (registeredUser) {
                    return registeredUser.update({profilePicture: imageBlob}).then(function (registeredUser) {
                        console.log("USER REGISTERED: " + JSON.stringify(registeredUser, null, 4));

                        var tmpUserStrings = JSON.stringify(registeredUser);
                        registeredUser = JSON.parse(tmpUserStrings);
                        delete registeredUser.password; // No need to send back password

                        emitMessageToEveryone('userRegistered', registeredUser);

                        res.end("Success");
                    });
                });
            }, function (err) {
                console.error("We got ERR, ", err.errors[0].message);
                res.end(err.errors[0].message)
            });
        });


    router.route('/friendsList/:userEmail')

        .get(function (req, res) {

            console.log("getAllFriends Function on server-post started --> myEmail = " + req.params.userEmail);

            UsersTable.find({where: {email: req.params.userEmail}}).then(function (user) {
                res.send(user.friendsList);

            });
        });

    router.route('/friends/:userEmail/:friendEmail')

        .post(function (req, res) {
            console.log("addFriend Function on server-post started --> myEmail = " + req.params.myEmail + " friendEmail = " + req.params.friendsList);

            var userAddingFriend;
            var userAddedAsFriend;

            return UsersTable.find({
                where: {
                    email: req.params.userEmail
                }
            }).then(function (user) {

                user.friendsList += req.params.friendEmail + separationSign;
                userAddingFriend = user;

                return UsersTable.update({
                    friendsList: userAddingFriend.friendsList
                }, {
                    where: {
                        email: req.params.userEmail
                    }
                }).then(function () {

                    return UsersTable.find({
                        where: {
                            email: req.params.friendEmail
                        }
                    }).then(function (user) {

                        user.friendsList += req.params.userEmail + separationSign;
                        userAddedAsFriend = user;

                        return UsersTable.update({
                            friendsList: userAddedAsFriend.friendsList
                        }, {
                            where: {
                                email: req.params.friendEmail
                            }
                        }).then(function () {

                            var tmpUserStrings = JSON.stringify(userAddedAsFriend);
                            userAddedAsFriend = JSON.parse(tmpUserStrings);
                            delete userAddedAsFriend.password; // No need to send back password

                            tmpUserStrings = JSON.stringify(userAddingFriend);
                            userAddingFriend = JSON.parse(tmpUserStrings);
                            delete userAddingFriend.password; // No need to send back password

                            emitMessageToSelf(req.params.userEmail, 'friendAdded', userAddedAsFriend);
                            emitMessageToSelf(req.params.friendEmail, 'friendAdded', userAddingFriend);

                            res.end("Success");
                        });
                    })
                });
            })
        })
        .delete(function (req, res) {
            console.log("removeFriend Function on server-delete started --> myEmail = " + req.params.userEmail + " friendEmail = " + req.params.friendEmail);


            var userRemovingFriend;
            var userRemovedAsFriend;

            return UsersTable.find({
                where: {
                    email: req.params.userEmail
                }
            }).then(function (user) {

                var oldFriendList = user.friendsList;
                var oldFriendListArray = oldFriendList.split(separationSign);
                oldFriendListArray.splice(oldFriendListArray.indexOf(req.params.friendEmail));

                var newFriendsList = "";

                for (var i = 0; i < oldFriendListArray.length; i++) {
                    newFriendsList += oldFriendListArray[i] + separationSign;
                }

                user.friendsList = newFriendsList;
                userRemovingFriend = user;

                return UsersTable.update({
                    friendsList: userRemovingFriend.friendsList
                }, {
                    where: {
                        email: req.params.userEmail
                    }
                }).then(function () {

                    return UsersTable.find({
                        where: {
                            email: req.params.friendEmail
                        }
                    }).then(function (user) {

                        var oldFriendList = user.friendsList;
                        var oldFriendListArray = oldFriendList.split(separationSign);
                        oldFriendListArray.splice(oldFriendListArray.indexOf(req.params.userEmail));

                        var newFriendsList = "";

                        for (var i = 0; i < oldFriendListArray.length; i++) {
                            newFriendsList += oldFriendListArray[i] + separationSign;
                        }

                        user.friendsList = newFriendsList;
                        userRemovedAsFriend = user;

                        return UsersTable.update({
                            friendsList: userRemovedAsFriend.friendsList
                        }, {
                            where: {
                                email: req.params.friendEmail
                            }
                        }).then(function () {

                            var tmpUserStrings = JSON.stringify(userRemovingFriend);
                            userRemovingFriend = JSON.parse(tmpUserStrings);
                            delete userRemovingFriend.password; // No need to send back password

                            tmpUserStrings = JSON.stringify(userRemovedAsFriend);
                            userRemovedAsFriend = JSON.parse(tmpUserStrings);
                            delete userRemovedAsFriend.password; // No need to send back password

                            emitMessageToSelf(req.params.userEmail, 'friendRemoved', userRemovedAsFriend);
                            emitMessageToSelf(req.params.friendEmail, 'friendRemoved', userRemovingFriend);

                            res.end("Success");
                        });
                    })
                });
            })
        });
    router.route('/addProfilePicture/')

        .post(function (req, res) {
            console.log("addProfilePicture Function on server-post started --> myEmail = " + req.body.email + " imagePath = " + req.body.path);

            var imageBlob = FS.readFileSync(req.body.path);
            //var imageBlob = FS.readFileSync("C:/Users/nir/Downloads/WebDevelopment/AfekaFace/app/components/images/" + req.body.email + ".png");

            UsersTable.update({
                    profilePicture: imageBlob
                }, {
                    where: {
                        email: req.body.email
                    }
                }
            ).then(function (user) {
                res.send(user);
            });
        });

    router.route('/getAllProfilePictures')

        .post(function (req, res) {

            console.log("\ngetAllProfilePictures Function on server-post:\n" + JSON.stringify(req.body, null, 4));

            var allProfilePictures = [], i, j;

            UsersTable.findAll().then(function (users) {
                for (i = 0; i < req.body.length; i++) {
                    for (j = 0; j < users.length; j++) {
                        if (req.body[i].user == users[j].email) {
                            allProfilePictures.push(users[j].profilePicture);
                            break;
                        }
                    }
                }
                res.send(allProfilePictures);
            });
        });

    router.route('/like/:postID/:userEmail')


        .post(function (req, res) {
            console.log("addLike Function on server-post started --> postID = " + req.params.postID + " newLikeLost = " + req.params.userEmail);

            var newLikeList;
            var isPostPrivate;

            return PostsTable.find({
                where: {
                    id: req.params.postID
                }
            }).then(function (post) {
                newLikeList = post.likeList + req.params.userEmail + separationSign;
                isPostPrivate = post.privatePermission;

                return PostsTable.update({
                        likeList: newLikeList
                    }, {
                        where: {
                            id: req.params.postID
                        }
                    }
                ).then(function (post) {
                    req.params.newLikeList = newLikeList;
                    emitMessageToSelf(req.params.userEmail, 'newLike', req.params);

                    //return getUserFromDBAsync(req.params.userEmail).then(function (user) {
                    if (!isPostPrivate)
                        return UsersTable.find({
                            where: {
                                email: req.params.userEmail
                            }
                        }).then(function (user) {
                            var parsedFriendsList = user.friendsList.split(separationSign);

                            emitMessageToFriends(parsedFriendsList, 'newLike', req.params);
                            res.end("Success");
                        })
                })
            })
        })


        .delete(function (req, res) {

            var newLikeList = "";
            var isPostPrivate;


            return PostsTable.find({
                where: {
                    id: req.params.postID
                }
            }).then(function (post) {

                var likeList = post.likeList.split(separationSign);
                likeList.splice(likeList.indexOf(req.params.userEmail), 1);
                isPostPrivate = post.privatePermission;

                if (likeList.length > 1)
                    for (var i = 0; i < likeList.length - 1; i++) {
                        newLikeList = newLikeList + likeList[i] + separationSign;
                    }

                return PostsTable.update({
                        likeList: newLikeList
                    }, {
                        where: {
                            id: req.params.postID
                        }
                    }
                ).then(function (post) {

                    req.params.newLikeList = newLikeList;
                    emitMessageToSelf(req.params.userEmail, 'removedLike', req.params);

                    //return getUserFromDBAsync(req.params.userEmail).then(function (user) {
                    if (!isPostPrivate)
                        return UsersTable.find({
                            where: {
                                email: req.params.userEmail
                            }
                        }).then(function (user) {
                            var parsedFriendsList = user.friendsList.split(separationSign);

                            emitMessageToFriends(parsedFriendsList, 'removedLike', req.params);
                            res.end("Success");
                        });
                });
            });
        });

    router.route('/savePost')

        .post(function (req, res) {

            try {

                return PostsTable.create(req.body).then(function (post) {
                    console.log("POST FROM DB IS: " + JSON.stringify(post, null, 4));

                    console.log("req.body.picture = " + req.body.picture);

                    try {

                        var imageBlob = FS.readFileSync(req.body.picture);
                    }
                    catch (exception) {
                    }
                    return post.update({picture: imageBlob}).then(function (post) {
                        return post.update({likeList: ""}).then(function (post) {

                            var tmpPostStrings = JSON.stringify(post);
                            post = JSON.parse(tmpPostStrings);

                            post.comments = [];

                            post = addDateOnPost(post);
                            post = addEmptyLikesToPost(post);

                            emitMessageToSelf(post.user, 'newPost', post);
                            if (!parseInt(post.privatePermission))
                                return UsersTable.find({
                                    where: {
                                        email: post.user
                                    }
                                }).then(function (user) {
                                    console.log("Not Private!")
                                    var parsedFriendsList = user.friendsList.split(separationSign);

                                    emitMessageToFriends(parsedFriendsList, 'newPost', post); // If post is public, send comment to friends also
                                    res.end("Success");
                                });
                            res.end("Success");
                        })
                    });
                });
            }
            catch (exception) {
            }
        });

    router.route('/addNewComment')

        .post(function (req, res) {


            console.log("commentData = " + JSON.stringify(req.body, null, 4));
            //console.log("commentData = " + req.params.comment);
            CommentsTable.create(req.body).then(function (comment) {
                console.log("commentData from DB = " + JSON.stringify(comment, null, 4));

                return PostsTable.find({ // Get the owning post
                    where: {
                        id: req.body.postID
                    }
                }).then(function (post) {
                    emitMessageToSelf(req.body.commentOwner, 'newComment', comment); // Send comment to self

                    if (!post.privatePermission) // Check if post is not private
                        return UsersTable.find({
                            where: {
                                email: req.body.commentOwner
                            }
                        }).then(function (user) {
                            var parsedFriendsList = user.friendsList.split(separationSign);

                            emitMessageToFriends(parsedFriendsList, 'newComment', comment); // If post is public, send comment to friends also
                            res.end("Success");
                        });
                    res.end("Success");
                });
            });
        });

    router.route('/isExists/:userEmail/:userPassword')

        .get(function (req, res) {

            console.log("isExists Function on server-post started --> email = " + req.params.userEmail + " password = " + req.params.userPassword);

            UsersTable.findOne({where: {email: req.params.userEmail}}).then(function (user) {

                var message;

                if (user == null) {
                    message = "0"; // no user found
                }
                else {

                    if (!passwordHash.verify(req.params.userPassword, user.password)) // incorrect password
                        message = "1";
                    else // all details correct
                        message = "2";
                }
                res.send(message);
            });
        });

    router.route('/getPassword/:userEmail/')

        .get(function (req, res) {

            console.log("getPassword Function on server-post started --> email = " + req.params.userEmail);

            UsersTable.find({where: {email: req.params.userEmail}}).then(function (user) {
                res.send(user);
            });
        });

    router.route('/getPosts/:myEmail/')

        .get(function (req, res) {

            console.log("getPosts Function on server-post started --> email = " + req.params.myEmail);

            PostsTable.findAll({where: {user: req.params.myEmail}}).then(function (posts) {
                res.send(posts);
            });
        });


    function updateCommentsOnPosts(posts) {

        var toType = function (obj) {
            return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
        };

        //var tmpPostsStrings = JSON.stringify(posts);
        //posts = JSON.parse(tmpPostsStrings);

        var promise = CommentsTable.findAll().then(function (comments) {

            var count = 0;
            for (var i = 0; i < posts.length; i++) {
                count = 0;
                posts[i].comments = [];
                for (var j = 0; j < comments.length; j++) {
                    if (posts[i].id == comments[j].postID) {
                        console.log("\nFound comment for post:\n");
                        posts[i].comments.push(comments[j]);
                        count++;
                    }
                }
            }

            return posts;
        });
        return promise;
    }

    router.route('/getSortedPosts/:myEmail/')

        .get(function (req, res) {

            console.log("getPosts Function on server-post started --> email = " + req.params.myEmail);

            //UsersTable.find({where: {email: req.params.myEmail}}).then(function (user) {

            return UsersTable.findAll().then(function (users) {
                var friendsList = [];
                var friend = "";
                var allPosts = [];
                var allUsers = users;
                var user;

                for (var i = 0; i < allUsers.length; i++) {
                    if (allUsers[i].email == req.params.myEmail) {
                        user = allUsers[i];
                        break;
                    }
                }

                var friendsText = user.friendsList;


                console.log("getSortedPosts Function on server-post started --> friendsText = " + friendsText);

                for (var i = 0; i < friendsText.length; i++) {

                    if (friendsText[i] != separationSign)
                        friend += friendsText[i];
                    else {
                        friendsList.push(friend);
                        friend = "";
                    }
                }

                friendsList.push(req.params.myEmail); // add my posts as well

                return PostsTable.findAll().then(function (posts) {

                    for (var j = 0; j < posts.length; j++)
                        for (var k = 0; k < friendsList.length; k++)
                            if (posts[j].user == friendsList[k]) {
                                if (posts[j].user == req.params.myEmail) // if it's my post, add it anyway
                                    allPosts.push(posts[j]);
                                else {
                                    if (!posts[j].privatePermission) // do not add a post that does not have public permission
                                        allPosts.push(posts[j]);
                                }
                            }

                    var tmpPostsStrings = JSON.stringify(allPosts);
                    allPosts = JSON.parse(tmpPostsStrings);


                    allPosts = addDateOnPostsArray(allPosts);
                    allPosts = addEmptyLikesToPosts(allPosts);

                    return PostsPicturesTable.findAll().then(function (pictures) {
                        for (var i = 0; i < allPosts.length; i++) {
                            allPosts[i].pictures = [];
                            for (var j = 0; j < pictures.length; j++) {
                                if (allPosts[i].id == pictures[j].postID) {
                                    allPosts[i].pictures.push(pictures[j].picture)
                                }
                            }
                        }

                        return updateCommentsOnPosts(allPosts).then(function (allPosts) {
                            console.log("\nbeforeeeee posts:\n" + JSON.stringify(allPosts, null, 4));
                            allPosts = addLikeList(allPosts, req.params.myEmail, allUsers);
                            console.log("\nfinal posts:\n" + JSON.stringify(allPosts, null, 4));
                            res.send(gatSortedArray(allPosts).reverse());
                        });
                    });
                });
            });
        });

    router.route('/users/:id')

        .get(function (req, res) {
            var users = siteData['users'];
            console.log("Get UserID req.params: " + req.params);
            console.log("Get UserID JSON req.params: " + JSON.stringify(req.params, null, 4));
            console.log("Get UserID req.params.id: " + JSON.stringify(req.params.id, null, 4));


            for (var i = 0; i < users.length; i++) {
                if (users[i].id == req.params.id) {
                    res.send(users[i]);
                }
            }
            res.end("No match found");

        })

        .delete(function (req, res) {

            /*        var index = siteData['users'].indexOf(req.body);
             siteData.splice(index, 1);*/

            /*var users = siteData['users'];
             console.log("Users: " + JSON.stringify(users, null, 4));
             var matchFound = false;
             var userDeleted;

             for (var i = 0; i < users.length; i++) {
             console.log("User[" + i + "] = " + JSON.stringify(users[i], null, 4));
             console.log("User[" + i + "] req.body = " + JSON.stringify(req.body, null, 4));
             if (users[i].id == req.params.id) {
             console.log("User[" + i + "] id matched!!");
             matchFound = true;
             userDeleted = users[i];
             users.splice(i, 1);
             break;
             }
             }

             if (matchFound) {
             siteData['users'] = users;
             console.log("Site Users: " + JSON.stringify(siteData, null, 4));
             res.send(userDeleted);
             }
             else {
             res.end("User not found");
             }*/

            console.log("Destroy Function on server-post started");
            var user = UsersTable.destroy({
                where: {
                    id: req.params.id
                }
            }).then(function (deletedUserID) {

                console.log("Deleted user id: " + JSON.stringify(deletedUserID, null, 4));
                console.log("Deleted user: " + JSON.stringify(user, null, 4));
                if (deletedUserID == 0)
                    res.end("User doesn't exist");
                else
                    res.end(deletedUserID);

            }, function (err) {
                console.error("We got ERR, ", err);
                res.end(err);
            });


        });


    /*    router.route('/pictures')

     .post(multipartyMiddleware, PhotosController.uploadFile);*/


    var uploadFiles = function () {
    };

    uploadFiles.prototype.uploadFile = function (req, res) {

        // We are able to access req.files.file thanks to
        // the multiparty middleware
        var file = req.files.file;
        console.log("Picture Uploaded!! " + JSON.stringify(file, null, 4))

        //var util = require('util');

        //console.log(util.inspect(req, {showHidden: false, depth: null}));


        var imageBlob = FS.readFileSync(file.path);

        return UsersTable.update({
                profilePicture: imageBlob
            }, {
                where: {
                    email: req.body.userEmail
                }
            }
        ).then(function (profilePicture) {

            return UsersTable.find({
                where: {
                    email: req.body.userEmail
                }
            }).then(function (user) {

                var data = [];
                data.push(user.email);
                data.push(imageBlob);

                var tmpUserStrings = JSON.stringify(user);
                user = JSON.parse(tmpUserStrings);

                delete user.password;

                //var userToSend = user;

                var parsedFriendsList = user.friendsList.split(separationSign);
                //console.log("User after picture update: " + JSON.stringify(user, null, 4));
                emitMessageToSelf(user.email, 'profilePictureChanged', user);
                emitMessageToFriends(parsedFriendsList, 'profilePictureChanged', user);
            });


        });


    };

    uploadFiles.prototype.uploadMultipleFiles = function (req, res) {

        // We are able to access req.files.file thanks to
        // the multiparty middleware
        var files = req.files.files;
        console.log("Picture Uploaded!! files: " + JSON.stringify(files, null, 4))
        var postData = req.body.postData;
        postData.likeList = "";
        console.log("Picture Uploaded!! postData: " + JSON.stringify(postData, null, 4))
        var postCreatedInDB;
        var pictures = [];


        return PostsTable.create(postData).then(function (post) {
            console.log("POST FROM DB IS: " + JSON.stringify(post, null, 4));

            var tmpPostStrings = JSON.stringify(post);
            post = JSON.parse(tmpPostStrings);

            post.comments = [];

            post = addDateOnPost(post);
            post = addEmptyLikesToPost(post);
            post.pictures = [];
            postCreatedInDB = post;
            var ArrayOfPhotosObjectsToAddToDB = [];
            pictures = [];
            if (!files) {
                emitMessageToSelf(postCreatedInDB.user, 'newPost', postCreatedInDB);
                if (!parseInt(postCreatedInDB.privatePermission))
                    return UsersTable.find({
                        where: {
                            email: postCreatedInDB.user
                        }
                    }).then(function (user) {
                        console.log("Not Private!");
                        var parsedFriendsList = user.friendsList.split(separationSign);

                        emitMessageToFriends(parsedFriendsList, 'newPost', postCreatedInDB); // If post is public, send comment to friends also
                        res.end("Success");
                    });
                res.end("Success");
            }
            else {

                for (var i = 0; i < files.length; i++) {
                    var temp = {};
                    pictures.push(FS.readFileSync(files[i].path));
                    temp.picture = pictures[i];
                    temp.postID = post.id;

                    ArrayOfPhotosObjectsToAddToDB.push(temp);
                }

                return PostsPicturesTable.bulkCreate(ArrayOfPhotosObjectsToAddToDB).then(function () {
                    for (var i = 0; i < pictures.length; i++) {
                        postCreatedInDB.pictures.push(pictures[i]);
                    }

                    console.log("POST FROM after creating photos IS: " + JSON.stringify(postCreatedInDB, null, 4));

                    var tmpPostStrings = JSON.stringify(postCreatedInDB);
                    postCreatedInDB = JSON.parse(tmpPostStrings);


                    emitMessageToSelf(postCreatedInDB.user, 'newPost', postCreatedInDB);
                    if (!parseInt(postCreatedInDB.privatePermission))
                        return UsersTable.find({
                            where: {
                                email: postCreatedInDB.user
                            }
                        }).then(function (user) {
                            console.log("Not Private!")
                            var parsedFriendsList = user.friendsList.split(separationSign);

                            emitMessageToFriends(parsedFriendsList, 'newPost', postCreatedInDB); // If post is public, send comment to friends also
                            res.end("Success");
                        });
                    res.end("Success");
                });
            }
        });
    };

    var uploadSingleFile = new uploadFiles();


    router.route('/profilePicture')

        .post(multipartyMiddleware, uploadSingleFile.uploadFile);

    router.route('/createPost')

        .post(multipartyMiddleware, uploadSingleFile.uploadMultipleFiles);


//other routes..

    function addLikeList(posts, myEmail, allUsers) {

        var userLiked;
        for (var i = 0; i < posts.length; i++) {

            posts[i].numberOfLikes = 0;

            if (!posts[i].likeList) // If no likes in post continue
                continue;

            posts[i].numberOfLikes++; // At least 1 like

            var likeListArray = posts[i].likeList.split(separationSign);

            console.log("Like List: " + JSON.stringify(likeListArray, null, 4))

            var myLikeIndex = likeListArray.indexOf(myEmail);
            var isLikedByMe = myLikeIndex != -1;

            posts[i].isLikedByMe = isLikedByMe;

            if (isLikedByMe) {
                likeListArray.splice(myLikeIndex, 1);
                posts[i].likes = "You";
            }
            else {
                userLiked = getUser(likeListArray[0], allUsers);
                likeListArray.splice(0, 1);
                posts[i].likes = userLiked.firstName + " " + userLiked.lastName;
            }

            for (var j = 0; j < likeListArray.length - 1; j++) {
                userLiked = getUser(likeListArray[j], allUsers);
                posts[i].likes += ", " + userLiked.firstName + " " + userLiked.lastName;
                posts[i].numberOfLikes++;
            }
        }
        return posts;
    }

    function getUser(userEmail, usersArray) {
        return usersArray[findUserIndexByEmail(userEmail, usersArray)];
    }

    // In order to find user index in users variable
    function findUserIndexByEmail(userEmail, usersArray) {
        var index = usersArray.map(function (user) {
            return user.email;
        }).indexOf(userEmail);
        return index;
    }

    function addEmptyLikesToPosts(posts) {
        for (var i = 0; i < posts.length; i++) {
            posts[i].likes = "";
            posts[i].numberOfLikes = 0;
        }
        return posts;
    }

    function addEmptyLikesToPost(post) {
        post.likes = "";
        post.numberOfLikes = 0;
        return post;
    }

    function addDateOnPostsArray(posts) {

        //var i, date;
        var i, j, allParts, dateParts, date, time;
        for (i = 0; i < posts.length; i++) {

            date = "";
            allParts = posts[i].createdAt.split("T");
            dateParts = allParts[0].split("-");

            for (j = dateParts.length - 1; j >= 0; j--)
                date += (j == 0) ? dateParts[j] : dateParts[j] + "/";

            time = allParts[1].split(".")[0];//.split(":");
            posts[i].date = date + " , " + time;
            //date = posts[i].createdAt;
            //var newDate = dates[i].getDate() + "/" + (dates[i].getMonth() + 1) + "/" + dates[i].getFullYear() + " , " + dates[i].getHours() + ":" + dates[i].getMinutes() + ":" + dates[i].getSeconds();
            //posts[i].date = newDate;
        }

        return posts;
    }

    function addDateOnPost(post) {

        var allParts;
        var dateParts;
        var date;
        var time;

        date = "";
        allParts = post.createdAt.split("T");
        dateParts = allParts[0].split("-");

        for (var j = dateParts.length - 1; j >= 0; j--)
            date += (j == 0) ? dateParts[j] : dateParts[j] + "/";

        time = allParts[1].split(".")[0];//.split(":");
        post.date = date + " , " + time;
        //date = posts[i].createdAt;
        //var newDate = dates[i].getDate() + "/" + (dates[i].getMonth() + 1) + "/" + dates[i].getFullYear() + " , " + dates[i].getHours() + ":" + dates[i].getMinutes() + ":" + dates[i].getSeconds();
        //posts[i].date = newDate;


        return post;
    }


    var posts = [];

    function sort() {

        var left = 0;
        var right = posts.length - 1;

        quickSort(left, right);
    }

    function quickSort(left, right) {

        if (left >= right)
            return;

        var pivot = posts[right];
        var par = partition(left, right, pivot);

        quickSort(0, par - 1);
        quickSort(par + 1, right);
    }

    function partition(left, right, pivot) {

        var leftCursor = left - 1;
        var rightCursor = right;

        while (leftCursor < rightCursor) {

            while (posts[++leftCursor].createdAt < pivot.createdAt);
            while (rightCursor > 0 && posts[--rightCursor].createdAt > pivot.createdAt);

            if (leftCursor >= rightCursor)
                break;
            else
                swap(leftCursor, rightCursor);
        }

        swap(leftCursor, right);
        return leftCursor;
    }

    function swap(left, right) {

        var temp = posts[left];
        posts[left] = posts[right];
        posts[right] = temp;
    }

    function gatSortedArray(array) {
        posts = array;
        sort();
        return posts;
    }

    function InitilizeDB(connection) {
        UsersTable = connection.define('user', {
            firstName: Sequelize.STRING,
            lastName: Sequelize.STRING,
            password: Sequelize.STRING,
            profilePicture: Sequelize.BLOB,
            friendsList: Sequelize.TEXT,
            //socketID: Sequelize.STRING,
            email: {
                type: Sequelize.STRING,
                unique: true
            }
        }, {
            hooks: {
                beforeDestroy: function (user, options) {
                    return user;
                }
            }
        });

        PostsTable = connection.define('post', {
            text: Sequelize.TEXT,
            privatePermission: Sequelize.BOOLEAN,
            picture: Sequelize.BLOB,
            user: Sequelize.STRING,
            likeList: Sequelize.TEXT

        }, {
            hooks: {
                beforeDestroy: function (post, options) {
                    return post;
                }
            }
        });

        CommentsTable = connection.define('comment', {
            comment: Sequelize.TEXT,
            commentOwner: Sequelize.STRING,
            postID: Sequelize.INTEGER

        }, {
            hooks: {
                beforeDestroy: function (comment, options) {
                    return comment;
                }
            }
        });

        PostsPicturesTable = connection.define('posts_picture', {
            picture: Sequelize.BLOB,
            postID: Sequelize.INTEGER
        });

        connection.sync();
        /*
         var imageData = FS.readFileSync('C:/Users/nir/Desktop/לימודים/mynir/china.jpg');

         UsersTable.create({
         profilePicture: imageData
         });

         */
    }

    app.use('/', router);
};